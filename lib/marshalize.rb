class Marshalize
  def initialize(args)
    if args.size == 4
      if args[0] == '-i' && args[2] == '-o'
        @path_input = args[1]
        @path_output = args[3]
        begin
          do_marshalize
        rescue
        end
      end
    end
  end
  
  private
  def do_marshalize
    File.open(@path_output, 'wb') {|f| f.write(Marshal.dump(File.read(@path_input)))}
    puts @path_output
  end
end
