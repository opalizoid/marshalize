class Unmarshalize
  def initialize(args)
    if args.size == 4
      if args[0] == '-i' && args[2] == '-o'
        @path_input = args[1]
        @path_output = args[3]
        begin
          do_unmarshalize
        rescue
        end
      end
    end
  end
  
  private
  def do_unmarshalize
    File.open(@path_output, 'w') {|f| f.write(Marshal.load(File.binread(@path_input)))}
    puts @path_output
  end
end
