Gem::Specification.new do |s|
  s.name        = 'marshalize'
  s.version     = '0.0.2'
  s.licenses    = ['MIT']
  s.summary     = "Pack and unpack to marshal binary file."
  s.description = <<-DESCRIPTION
  marshalize -i [input] -o [output]; unmarshalize -i [input] -o [output]
  DESCRIPTION
  s.author      = ["Łukasz Fuszara"]
  s.email       = 'opalizoid@gmail.com'
  s.date        = '2016-11-21'
  s.homepage    = 'https://rubygems.org/gems/marshalize'
  s.required_ruby_version = '>= 2.3.1'
  
  s.files       = `git ls-files`.split("\n")
  s.executables = ['marshalize', 'unmarshalize']
  s.require_path = ['lib']
end
